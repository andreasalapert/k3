import java.util.*;


public class DoubleStack {

    public static void main(String[] argum) {
        System.out.println(interpret("2 5 9 ROT + SWAP -"));
    }

    private LinkedList<Double> stack;

    DoubleStack() {
        stack = new LinkedList();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {

        DoubleStack clonedStack = new DoubleStack();
        for (int i = 0; i < size(); i++) {
            clonedStack.push(stack.get(i));
        }
        return clonedStack;
    }

    public boolean stEmpty() {
        return stack.size() == 0;
    }

    public void push(double a) {
        stack.addLast(a);
    }

    private int size() {
        return stack.size();
    }

    public double pop() {
        if (size() == 0) throw new RuntimeException("Stack is already empty!");
        return stack.removeLast();
    }


    public double get(int index){

        double elementAtIndex;
        try{ elementAtIndex = stack.get(index); }
        catch (IndexOutOfBoundsException e) { return Double.NaN; }

        return elementAtIndex;
    }

    public void op(String s) {

        if (size() < 2)
            throw new RuntimeException("Not enough numbers to perform the required operation");

        else {
            double operandTwo = pop();
            double operandOne = pop();

            switch (s) {
                case "+":
                    push(operandOne + operandTwo);
                    break;
                case "-":
                    push(operandOne - operandTwo);
                    break;
                case "*":
                    push(operandOne * operandTwo);
                    break;
                case "/":
                    push(operandOne / operandTwo);
                    break;
                default:
                    throw new RuntimeException(String.format("%s is not on the list of accepted operators %s", s, "[+, -, *, /]"));
            }
        }
    }

    public double tos() {

        if (size() == 0) throw new RuntimeException("Stack is already empty!");
        return stack.getLast();
    }

    @Override
    public boolean equals(Object o) {

        if (this.size() != ((DoubleStack) o).size())
            return false;

        for (int i = 0; i < stack.size(); i++) {
            if (get(i) != ((DoubleStack) o).get(i))
                return false;
        }

        return true;
    }

    @Override
    public String toString() {

        String stackString = "";
        StringBuffer buffer = new StringBuffer(stackString);
        for (Iterator i = stack.iterator(); i.hasNext(); ) {
            buffer.append(String.format("%s ", i.next()));
        }
        return buffer.toString().trim();
    }

    public static boolean isNumeric(String str) {

        try { Double.parseDouble(str); }
        catch (Exception e) { return false; }

        return true;
    }

    public static double interpret(String pol) {

        String trimmedEquation = pol.trim();
        if (trimmedEquation.length() == 0)
            throw new RuntimeException("Given equation is empty!");

        List<String> acceptedOperators = new ArrayList<String>(){{
            add("+");
            add("-");
            add("*");
            add("/");
            add("SWAP");
            add("ROT");
        }};


        String[] splittedEquation = trimmedEquation.split("\\s+");
        DoubleStack stack = new DoubleStack();

        for (String currentElement : splittedEquation) {
            if (isNumeric(currentElement)) {
                double number = Double.parseDouble(currentElement);
                stack.push(number);
            }
            else {
                String operator = currentElement;
                if (!acceptedOperators.contains(operator))
                    throw new RuntimeException(String.format("%s is not on the list of accepted operators [%s] in equation %s", operator, acceptedOperators, trimmedEquation));

                if (stack.size() < 2)
                    throw new RuntimeException(String.format("Not enough numbers to perform operation %s in equation %s", operator, trimmedEquation));

                if(operator.equals("SWAP")){
                    double a = stack.pop();
                    double b = stack.pop();

                    stack.push(a);
                    stack.push(b);
                    continue;
                }

                if(operator.equals("ROT")) {
                    double a = stack.pop();
                    double b = stack.pop();
                    double c = stack.pop();

                    stack.push(b);
                    stack.push(a);
                    stack.push(c);

                    continue;
                }
                stack.op(operator);
            }
        }
        if (stack.size() > 1)
            throw new RuntimeException(String.format("Too many numbers in equation  %s", trimmedEquation));

        return stack.tos();
    }
}

